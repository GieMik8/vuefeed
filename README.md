# vuefeed

> RSS Feed Reader in Vue.js 2

## Build Setup

This RSS Feed Reader made by G.M. for testing & improving purposes. In order to build this api you need to install
Node in your machine.

``` bash
# install Bower globally
npm install -g bower

# install Gulp globally (optional - only for style editing)
npm install -g gulp

# install dependencies
npm install

# install bower dependencies
bower install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
