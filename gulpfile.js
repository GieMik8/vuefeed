var gulp = require('gulp');
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');

var source = 'style/scss/**/*.scss';
var dest = './style/';

function onError(err) {
    console.log(err);
}

gulp.task('default', function(){
    return gulp.src(source)
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(prefix())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(dest))
    .pipe(plumber({
        errorHandler: onError
    }));
});

gulp.task('watch', function () {
    gulp.watch(source, ['default']);
});