import Vue from 'vue'
import App from './App.vue'
// import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import VeeValidate from 'vee-validate';
// import { routes, titles } from './routes';
import Storify from './storify.js';

// Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VeeValidate);
Vue.use(Storify);

export const alertEvents = new Vue();

// const router = new VueRouter({
//   routes,
//   mode: 'history',
// });

// export const feedsData = new Vue();

// router.afterEach((toRoute, fromRoute) => {
//     window.document.title = 'VueReader - '+titles[toRoute.name];
//     window.scrollTo(0, 0);
// })

new Vue({
  el: '#app',
  // router,
  render: h => h(App)
})
