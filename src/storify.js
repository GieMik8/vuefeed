const Storify = {};

Storify.install = function(Vue){
    var urlHistory = [];

    var services = {
        
        // Sets localStorage
        setLocal(name, value) {
            if (typeof(value) === 'string') {
                localStorage.setItem(name, value);
            } else {
                alert('given value to set local storage is not a string');
            }
        },

        // Gets localStorage
        getLocal(name) {
            if (localStorage.getItem(name)) {
                return localStorage.getItem(name);
            } else {
                return false;
            }
        },

        // Returns history
        getHistory(name) {
            var temp = this.getLocal(name) || [];

            if (temp.length > 0) {
                var cHistory = temp.split('|');
            } else {
                var cHistory = [];
            }

            return cHistory;
        },

        clearHistory(name) {
            localStorage.removeItem(name);
        },

        // Adds value to history array, and sets localStorage
        add(storageName, value) {
            var temp = this.getHistory(storageName);
            if (temp.includes(value)) {
                let index = temp.indexOf(value);
                let tempValue = temp[index];
                temp.splice(index, 1);
                temp.unshift(tempValue);
            } else {
                temp.unshift(value);
            }
            this.setLocal(storageName, temp.join('|'));
        },
    }

    Vue.prototype.$storage = services;
};

export default Storify;